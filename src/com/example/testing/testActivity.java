package com.example.testing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import android.util.Log;

import android.app.Activity;
import android.app.ActivityManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;

public class testActivity extends Activity {
	/** Called when the activity is first created. */
    @Override
	public void onCreate(Bundle savedInstanceState){    
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_test);
		int SleepPid = -1;
		
		ActivityManager manager = (ActivityManager)this.getSystemService(ACTIVITY_SERVICE);
		List<ActivityManager.RunningAppProcessInfo> list = manager.getRunningAppProcesses();
		if (list!= null){
			for (int i = 0; i < list.size(); ++i){
				if ("com.ntu.mhci.iLoveSleep".matches(list.get(i).processName)){
					SleepPid = list.get(i).pid;
				}
			}
		}
		final File vSDCard =  Environment.getExternalStorageDirectory();
		final String separator = System.getProperty("line.separator"); 
		final int SleepPid2 = SleepPid;
		
		
		new Thread(){
			public void run(){
				try{
					Process process = Runtime.getRuntime().exec("logcat");
		            final BufferedReader bufferedReader = new BufferedReader(
		            new InputStreamReader(process.getInputStream()));
				
					String line = "";
					
					FileWriter vFile = new FileWriter(vSDCard.getAbsolutePath() + "/SleepLog.txt", false);
					
					vFile.write("PID = " + SleepPid2);
					vFile.write(separator);
					Log.e ("testing", "pid = " + SleepPid2);
					
					while (true){
						if((line = bufferedReader.readLine())!=null){
							if (line.contains((SleepPid2+")"))){
								vFile.write(line);
								vFile.write(separator);
							}
						}
						else{
							Thread.sleep(1000);
						}
					}
				}catch(IOException e) {		
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}.start();		
	}
}